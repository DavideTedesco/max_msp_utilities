declare filename "MS.dsp"; declare name "MS"; declare compilation_options    "-single -scal -I libraries/ -I project/ -lang wasm";
declare library_path "FaustDSP";
declare library_path "/libraries/stdfaust.lib";
declare filename "FaustDSP";
declare name "FaustDSP";
process = _,_<:(0.5f,(_,_ : +) : *),(0.5f,(_,_ : -) : *);
